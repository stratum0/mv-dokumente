---
vim: ts=4 et sw=4 tw=90
build-me-with: pandoc -f markdown+grid_tables -i protokoll-mv-2019-01-20.md -s -t latex -o protokoll-mv-2019-01-20.pdf ----template=pandoc-template
documentclass: s0minutes
papersize: a4
lang: de
toc: true
toc-depth: 2
links-as-notes: true
title: Mitgliederversammlung 2019-01-20
s0minutes:
  typeofmeeting: \generalassembly
  date: 2019-01-20
  place: Stratum 0, Hamburger Straße 273a, Braunschweig
  startingtime: 14:00
  endtime: 17:07
  attendants: 34 ordentliche Mitglieder, 0 Fördermitglieder, 1 Gast
  absentees: 
  minutetaker: rohieb
---

Protokoll-Overhead
==================

Wahl der Versammlungsleitung:
Es wird larsan vorgeschlagen.
Es gibt sehr große Zustimmung für larsan.
larsan leitet somit die Versammlung.

Wahl der Protokoll führenden Entität:
Es wird rohieb vorgeschlagen.
Es gibt sehr große Zustimmung und eine Enthaltung für rohieb.
rohieb schreibt also ein Protokoll.

Zulassung von Gästen:
Es ist 1 Gast anwesend.
Es wird gefragt, ob Gäste zugelassen werden sollen.
Es gibt 1 Gegenstimme und 2 Enthaltungen.
Gäste werden somit zugelassen.

Feststellung der Beschlussfähigkeit:
Der Verein hat zum heutigen Stichtag 10 Fördermitglieder und 94 ordentliche
Mitglieder, insgesamt also 104 Mitglieder.
Satzung, §7 Abs. 6 fordert Anwesenheit von mindestens 23% der gesamten
Mitglieder für eine beschlussfähige Mitgliederversammlung.
23% von 94 sind 21,62.
Es sind 32 ordentliche Mitglieder anwesend.
Das ist mehr als 21,62.
Die Versammlung ist somit beschlussfähig.

Das Quorum für die Annahme eines Antrags (Satzung, §7 Abs. 7) beträgt mehr als
50% der anwesenden ordentlichen Mitglieder.
50% von 32 sind 16.
Ein Antrag gilt also mit 17 Pro-Stimmen als angenommen.

Berichte
========

Finanzbericht
-------------

Emantor berichtet als Schatzmeister über die Finanzen des Vereins, und verweist auch auf
das laufend aktualisierte Finanz-Portal.[^ds0f]  Sein Finanzbericht ist auf der
Homepage[^finanz2018] verfügbar, und wird hier auszugsweise mit den mündlichen Anmerkungen
wiedergegeben.

[^ds0f]: <https://data.stratum0.org/finanz/>
[^finanz2018]: <https://stratum0.org/wiki/Datei:Finanzbericht_MV_2019.pdf>

\paragraph{Kontenübersicht}

\begin{longtable}{llr}
  \textbf{Nr.} & \textbf{Name} & \textbf{Bestand zum 31.12.2018 [€]} \\
  \midrule
  \endfirsthead
  100          & Barkasse                              &    1.934,43 \\
   100-1       & Spenden für 3D-Drucker Filament       &        0,00 \\
   100-2       & Pfand für physische Schlüssel         &      460,00 \\
   100-3       & Spenden für Plotter-Material          &        0,00 \\
   100-4       & Spenden für Stick-Material            &      179,91 \\
  \midrule
  101          & Erstattungskasse Verbrauchsmaterial   &       54,25 \\
  \midrule
  102          & Matekasse                             &      217,54 \\
  \midrule
  200024917    & Girokonto                             &   12.249,62 \\
   200024917-1 & Rückstellungen Girokonto              &    3.160,00 \\
  \midrule
  \midrule
               &  \textbf{Summe}                & \textbf{ 18.255,75 } \\
\end{longtable}

_Anmerkung:_ Laut Aussage des Schatzmeisters enthält die Pfandkasse für Schlüssel 460€,
und nicht 360€, wie irrtümlich im veröffentlichten Finanzreport angegeben.

Die Stickmaterialkasse ist noch nicht aufgelöst, weil noch zweckgebundene Spenden darauf
gebucht sind. Die Matekasse ist nicht die Blechdosen-Matekasse, sondern nur die grüne,
mit „Matekasse-Backbone“ beschriftete Kasse erscheint in der Buchführung.  Von der
Blechdosen-Vertrauenskasse gibt es regelmäßige Transfers in den Backbone.

\paragraph{Neues seit 2018}

Wie immer hat sich der Schatzmeister viel vorgenommen… aber es geht voran.  Leider sind
die Vorstands-Arbeitstreffen mangels Beteiligung (von Seiten der Vorstände als auch der
Community) eingeschlafen, aber die neu eingeführten vierteljährliche Kassenprüfungen haben
zu Entlastung geführt und sind entspannter. Hierzu wird eindeutig eine Empfehlung für den
zukünftigen Vorstand ausgesprochen.

\paragraph{Nackte Zahlen}

Die Zahlen beziehen sich auf das komplette Jahr vom 01.01.2018 bis 31.12.2018:

\begin{longtable}{lr>{\textcolor{red}\bgroup}r<{\egroup}r}
  \textbf{Bereich} & \textbf{Einnahmen [€]} & \textbf{Ausgaben [€]} & \textbf{Saldo [€]} \\
  \midrule
  \endfirsthead
  \multicolumn{3}{c}{\emph{(Fortsetzung von vorheriger Seite)}} \\
  \\
  \textbf{Bereich} & \textbf{Einnahmen [€]} & \textbf{Ausgaben [€]} & \textbf{Saldo [€]} \\
  \midrule
  \endhead
  \\
  \multicolumn{3}{c}{\emph{(Fortsetzung auf nächster Seite)}} \\
  \endfoot
  \endlastfoot
  Ideeller Bereich: Allgemein       & 20.101,91 &  –1.134,35   &  18.967,56 \\
   davon Mitgliedsbeiträge          & 17.550,48 &      –0,00   &            \\
   davon Spenden                    &  2.279,59 &      –0,00   &            \\
   davon allgemeine Ausgaben        &      0,00 &    –168,54   &            \\
   davon Kontoführungsgebühren      &      0,00 &    –212,23   &            \\
   davon Pfandgeld Schlüssel        &    120,00 &     –20,00   &            \\
   davon Vereinsserver              &      0,00 &    –733,58   &            \\
   davon Bekleidung                 &    151,84 &      –0,00   &            \\
  \midrule
  Ideeller Bereich: Projekte        &    528,12 &  –6.213,17   &   5.695,05 \\
   davon Bastelmaterial             &      0,00 &    –891,62   &            \\
   davon Stickmaschine              &     51,62 &      –0,00   &            \\
   davon Schneidplotter             &     68,20 &    –166.36   &            \\
   davon 3D-Drucker                 &    104,30 &    –184,62   &            \\
   davon Freifunk                   &    240,00 &  –4.970,57   &            \\
   davon Schweißen                  &     50,00 &      –0,00   &            \\
   davon CoderDojo                  &     14,00 &      –0,00   &            \\
  \midrule
  Ideeller Bereich: Space           &    110,52 & –14.921,28   & \textcolor{red}{-14.810,76} \\
   davon Rundfunkgebühren           &      0,00 &      –0,00   &            \\
   davon Miete und Nebenkosten      &    110,52 & –13.702,81   &            \\
   davon Verbrauchsmaterial         &      0,00 &     –93.56   &            \\
   davon Einrichtung                &      0,00 &  –1.124,91   &            \\
  \midrule
  Vermögensverwaltung               &      0,00 &      –0,00   &       0,00 \\
  \midrule
  Zweckbetriebe                     &      0,00 &     –64,36   & \textcolor{red}{-64,36} \\
  \midrule
  Wirtschaftlicher Geschäftsbetrieb &  5.157,73 &  –4.715,23   &     442,50 \\
   (Matekasse)                      &           &              &            \\
  \midrule
  Mankobuchungen                    &     55,94 &      –1,15   &      54,79 \\
  \midrule
  \midrule
  \textbf{Gesamt}                   & \textbf{25.954,22} &  \textbf{26.467,06}   & \textbf{\textcolor{red}{-512,84}} \\
\end{longtable}

Die Spendeneinnahmen beinhalten 1.000€ von quuxlogic, 1.000€ von Pengutronix, sowie etwa
80€ über Amazon Smile.

Die Projekte tragen sich halbwegs selbst, der Verein muss ein bisschen bezuschussen, aber
das findet Emantor akzeptabel.  Die Einnahmen in dieser Kategorie sind an den Spendenboxen
gemessen, die an den einzelnen Orten stehen.  An Material für die Stickmaschine muss im
Moment nichts nachgekauft werden, aber es kommen weiterhin anonyme Spenden dafür rein.
Die Freifunk-Kategorie beinhaltet die Förderung der Stadt Braunschweig aus dem letzten
Jahr von 4500 € als durchlaufenden Posten.  Jedoch hätten wir gerne noch mehr
Freifunk-Spenden, um die Server-Infrastruktur besser zu bezahlen, die im Moment teilweise
von Privatleuten bezahlt wird. Es bestehen noch 50€ zweckgebundene Spenden für Schweißen,
und 14€ zweckgebunden für CoderDojo.  Ideen für Anschaffungen in den zweckgebundenen
Bereichen sind gerne gesehen.

Die Kategorie „Space“ beinhaltet alle Kosten, um die Räumlichkeiten aufrecht zu erhalten.
Hier gab es eine Rückzahlung von 110€ von Naturstrom für zu viel gezahlte Stromkosten.  Als
Einrichtung wurde ein Schrank für Vorstandsdokumente angeschafft, weitere SAMLA-Boxen von
IKEA, einige Festplatten für das NAS im Space, eine APU als Router und Service-VM im
Space, sowie die Textil-Ecke renoviert.  Unter Verbrauchsmaterial fällt z.B.
Toilettenpapier, Reinigungsmittel.

Im Bereich „Zweckbetriebe“ wurden 64€ für den Transport zum 34c3 in Leipzig ausgegeben.

Der Wirtschaftliche Geschäftsbetrieb (Getränkekasse) zeigt dieses Jahr relativ wenig
Gewinn, im letzten Jahr wurde ungefähr die selbe Menge in einem 3-Monats-Zeitraum
umgesetzt.  Der Schatzmeister vermutet, dass für Flora Mate eher 1€ als (wie
angesetzt) 1,50€ als Vertrauenspreis pro Flasche gezahlt wird.  Die Getränke-Strichlisten
sind insgesamt relativ ausgeglichen, auch wenn es einige Leute mit –100 sowie mit +100
Strichen gibt.  Hier herrscht also eine starke Streuung.

Eine Mankobuchung von 1,15€ ist entstanden, weil die Barkasse in der Werkstatt
heruntergefallen ist und dadurch ein paar Münzen verloren hat.  Dies ist wird allerdings durch die
zusätzlichen, zu wenig verbuchten 55€ in der Matekasse mehr als ausgeglichen.

Der Gesamtsaldo zeigt dieses Jahr einen Verlust von 512€.  Hierbei ist allerdings zu
berücksichtigen, dass die Freifunk-Förderung der Stadt Braunschweig im letzten Jahr
eingenommen, aber erst in diesem Jahr ausgegeben wurde.  Ohne diesen durchlaufenden Posten
ergibt sich also eher ein Überschuss von etwa 4.000€.

\paragraph{Gegenüberstellung}

Durchschnittlich wurden im letzten Jahr pro Monat mehr als 1.869€ eingenommen:

* 1.587€ Mitgliedsbeiträge
* 262€ zweckgebundene Spenden
* sowie jeweils zwischen 0 und 40€ aus den entsprechenden anonymen Spendenboxen für
  3D-Druck, Plotter, und Stickmaschine. Das Spendenaufkommen schwankt hierbei je nach
  Nutzung.
* Freifunk wird zweckgebunden von zwei Personen mit insgesamt 20€ pro Monat unterstützt.

Dem gegenüber stehen monatliche Grundausgaben von 1.145€:

* 730€ Miete (inkl. Nebenkostenabschlag)
* 268€ Strom (Abschlag)
* 40€ Internet
* 60€ Vereins-Server
* 33€ Freifunk-Infrastruktur
* 12€ Haftpflicht-Versicherung
* 1€ Domain: stratum0.org

Zu berücksichtigen sind außerdem buchungsabhängige Kontoführungsgebühren, die etwa 20€ pro
Monat ausmachen.

Die Matekasse erzeugt weiterhin konstante Einnahmen, die die Ausgaben aufwiegen.
Im September wurde ein großer Teil der übrig gebliebenen Flora-Mate vom HOA-Camp von der
SMFW UG aufgekauft, was sich als große Ausgabe widerspiegelt.

\paragraph{Mitgliederentwicklung}

Im Moment gibt es relativ wenig Zuwachs, über das Jahr ist die Mitgliederzahl von 101 auf
103 gestiegen, ging zwischendurch auch mal bis auf 99 hinunter.
Es wurde auch kaum Werbung im letzten Jahr gemacht – in den vorangegangenen Jahren gab es
regelmäßige Kooperation mit der Fachgruppe Informatik der TU Braunschweig. Dem
entsprechend gibt es dieses Jahr wenig Studenten als Neumitglieder.

Es wird gefragt, wie sehr der verfügbare Platz im Space sich auf die Mitgliederentwicklung
auswirkt. Der Schatzmeister sieht den Platzbedarf im Space im Moment als unkritisch und
verweist auf spätere TOPs.

\paragraph{Rückstellungen}

Weiterhin besteht eine Rückstellung von 3.000€, um eventuelle Ausfälle bei den Einnahmen 
auffangen zu können und laufende Kosten zu decken.
Die Rückstellung zur Mietsicherheit müsste nochmal aufgelöst werden – dies wurde vom
Vorstand schon länger beschlossen, ist aber in der Finanzbuchhaltung noch nicht umgesetzt.

\paragraph{Ausblick}

Der Stromverbrauch im Space bewegt sich weiterhin auf hohem Niveau jenseits der 10
MWh/Jahr.

Großspenden von Firmen wurden für 2019 weiterhin zugesagt.

Nebenkosten 2016 wurden inzwischen bezahlt, und der monatliche Abschlag um 100€ erhöht.

Weiterhin besteht das Problem, dass zweckgebundene Spenden innerhalb von zwei Jahren
verwendet werden müssen. Dies betrifft insbesondere die Stickmaterial-Kasse. Der
Schatzmeister weist darauf hin, dass eventuelle Anschaffungen auch größer als der
zweckgebundene Betrag sein können, der Verein würde dann den Rest zuschießen.
Ideen für Anschaffungen sind immer gern gesehen, bitte wendet euch an den Vorstand.

Es wird angemerkt, dass für einen eventuellen Umzug in größere Räumlichkeiten eine
Rücklage gebildet werden kann.

Es wird die Frage gestellt, ob digitale Zahlungsformen (PayPal, Überweisung) zur Deckung
der Getränkekasse genutzt werden können. Aus Sicht des Schatzmeisters ist dies aber
buchungstechnisch kompliziert, zwischen Finanzbuchhaltung und Strichliste zu
synchronisieren.

Der Schatzmeister weist darauf hin, dass Barzahlung weniger Transaktionsgebühren
verursacht als Bank-Überweisungen.

Bericht der Rechnungsprüfer
---------------------------

Angela und shoragan berichten als Rechnungsprüfer.

Dieses Jahr gab es kleinere, wiederholte Kassenprüfungen über das Jahr verteilt. Im Großen
und Ganzen zeigt sich ein gutes Bild. Die Zahlen der Finanzbuchhaltung sind von den
beiden Rechnungsprüfern nachvollziehbar und es gibt keine Bedenken von ihrer Seite.

Aus Sicht der Rechnungsprüfer gibt es einige TODOs, die gelöst werden sollten, aber dies
betrifft keine größeren Beträge. Falls beispielsweise für eine Anschaffung kein Beleg (aus
China) existiert, muss trotzdem von derjenigen Entität, die die Auslage getätigt hat, ein
Ersatzbeleg erstellt werden.

Es wird angemerkt, dass es viele ermäßigt zahlende Mitglieder gibt, die nach Wissen der
Rechnungsprüfer im Moment nicht mehr studieren oder wo der Grund für die Ermäßigung
anderweitig weggefallen ist. Diese ermäßigt zahlenden Mitglieder sollten einmal in sich
gehen, und überlegen, ob sie nicht den vollen Mitgliedsbeitrag zahlen wollen.

Außerdem wird angemerkt, dass eine quartalsweise, halb- oder jährliche Überweisung des
Mitgliedsbeitrags Transaktionsgebühren auf Vereinsseite spart. Es könnte sich für den
Verein außerdem lohnen, bei der Bank ein anderes Kontenmodell zu wählen, dies sollte
evaluiert werden.

Alle Entitäten werden ermahnt, die Kulanzregelung von 10 offenen Strichen auf der
Getränke-Strichliste nicht auszureizen. Die Tatsache, dass einige Entitäten dort bis zu
100 Euro im Minus sind ist einfach nicht fair gegenüber anderen Nutzern.

Bei der Matekasse (Backbone) gab es eine Buchung im Kassenbuch, zu der kein Bon
existierte. Die Rechnungsprüfer verweisen hier auf die innenliegende Dokumentation oder auf
Leute, die sich auskennen und im Zweifel gefragt werden sollen.

Mit dem abschließenden Hinweis, dass der nächste Schatzmeister sich um die angesprochenen
TODOs kümmern sollte, empfehlen sie die Entlastung des Schatzmeisters.

Jahresbericht
-------------

larsan fasst das Jahr aus Sicht des Vereins zusammen. Seine Präsentation mit vielen
Bildern ist ebenso auf der Homepage zu finden[^jb2019].

[^jb2019]: <https://stratum0.org/wiki/Datei:JahresberichtMV2019.pdf>

\paragraph{Gruppen und regelmäßige Veranstaltungen}

Es gab dieses Jahr etwa 20-30 regelmäßige Termine pro Monat, dazu gehörten Vorträge, CoderDojo,
Digitalcourage, Freifunk, Coptertreffen, Home Automation, Malkränzchen, Anime Referat,
und Captain's Log.

Vorträge am 14. eines Monats haben sich etabliert, es gab im letzten Jahr 31 Vorträge an
10 Terminen. Davon wurden 24 Vorträge aufgezeichnet, und an einem Live-Streaming-Setup
wird weiterhin geschraubt.

Das CoderDojo fand wie geplant an 11 Termine statt. Der (kostenlose) Ticketverkauf ist von
Eventbrite auf ein eigenes Pretix auf einem Vereinsserver umgezogen, und Tickets sind
regelmäßig ausverkauft. Deswegen werden wie immer Mentoren gesucht – kommt einfach vorbei
oder meldet euch auf der Mailingliste.[^cdml] Im kommenden Jahr gibt es vermutlich wieder
externe Veranstaltungen in Kooperation mit Firmen. 

[^cdml]: <https://lists.stratum0.org/mailman/listinfo/dojoorga>

Freifunk trifft sich weiterhin jeden Mittwoch, und das Netz hält sich bei etwa 330 Routern
und in der Spitze 820 Clients (etwas weniger als im letzten Jahr). Das Netz wird am Leben
gehalten, aber es sind zukünftige Verbesserungen und Umbauten geplant (siehe dazu auch den
Talk auf dem 35c3[^35c3oio]). Es existiert weiterhin eine Kooperation mit der Stadt
Braunschweig, und eine Kooperation mit der TU Braunschweig ist geplant. Deswegen werden
Laute gesucht, die sich bei Freifunk um verschiedene Facetten wie Technik, Administration,
politische Arbeit, Klinken putzen bei Unternehmen oder Kommunikation kümmern wollen.

[^35c3oio]: <https://media.freifunk.net/v/35c3oio-69-project-parker-klassisches-routing-fr-freifunk>

Einige Workshops zum Bier brauen und Wurst machen fanden statt. Die Vegan Academy
(gemeinsam vegan kochen) fand nach Bedarf statt, ist aber eher eingeschlafen.

\paragraph{Infrastruktur}

Nach mehreren Anläufen gibt es jetzt endlich gibt es Internet mit 100 MBit/s, und
Glasfaser (zwischen Serverschrank und Frickelraum). Das NAS im Space (_trokn_) hat ein
Speicher-Upgrade bekommen (für welches ein Crowdfunding gestartet wurde, was aber nie
eingesammelt wurde, was aber dem Verein auch nicht weh tut). Jemand hat einen neuen
Drucker im Space abgeladen, der kann alles (außer lochen). Die Spacekiste (frei
verfügbarer Rechner im Chillraum) hat ein Upgrade auf v4 bekommen.

Der Vereinsserver wird rege genutzt; dort wurde ein Pretix für Ticketverkäufe aufgesetzt
(für Zugriff larsan ansprechen). Der RAM wird dort langsam knapp, für das gleiche Geld
wäre inzwischen auch mehr Leistung verfügbar, aber ein Umzug ist Aufwand.

\paragraph{Events}

Auch 2018 waren wir mit einem Stand auf der Maker Faire Hannover vertreten. Ebenso gab es
Präsenzen auf der EasterHegg (Würzburg), GPN18 (Karlsruhe), Hackover18 (Hannover), GPN18.5
(Karlsruhe), 35c3 (Leipzig), und ein paar weiteren.

Ein Highlight war dieses Jahr wieder das Hacken-Open-Air-Camp in Rötgesbüttel, was von
unserer Community organisiert wurde. Nächstes Jahr wird es keine Ausgabe des HOA geben, da
stattdessen wieder das CCC-Camp ansteht.

Eine Silvesterfeier fand mit etwa 35 Leuten im Space statt.

\paragraph{Kurzmeldungen}

* Stratumnews sind eingeschlafen, sollte man wieder aufnehmen
* Workshops: leider außer Wurst und Bier keine dieses Jahr (die uns bekannt sind, oder auf
  jeden Fall zu wenig!)
* Space-Bau-Abende: es fand einer statt, aber wir sind auch auf der maximalen Ausbaustufe
* larsan hat gezählt: es gibt 173 SAMLA-Boxen im Space verteilt
* Hoverboards. Hoverboards Hoverboards Hoverboards. Mit Crashkurs. (Die Wand im Flur wurde
  von uns inzwischen wieder repariert.)
* Wir haben Stratum Quo erreicht… siehe auch TOP unten. Es gibt keine großen Peaks, keine großen Veränderungen, finanziell ists okay, aber es passiert nichts Großes mehr (vielleicht bis auf das HOA-Camp)

\paragraph{Ausblick 2019}

Im kommenden Jahr steht wieder ein CCC-Camp an, ein Planungstreffen findet im Anschluss an
diese Mitgliederversammlung statt. Außerdem werden wir 2019 0b1000 (010, 0x8) Jahre alt,
was gefeiert werden sollte.

Ein größerer Space wird weiterhin gesucht. Gegenüber an der Hamburger Straße gäbe es z.B.
so eine Lagerhalle, die zu Berg und Sohn / Autohaus Holzberg gehört, und zusehends
verfällt. Spontan meldet sich jemand, der Kontakt zu deren Verkaufsleiter hat und dort mal
anfragen würde. (Außerdem wird angemerkt, dass die Burgpassage frei wird…)

Tätigkeitsbericht des Vorstands
-------------------------------

Was hat der Vorstand je für uns getan? Diese Frage beantwortet ebenso larsan.

\paragraph{Tagesgeschäft}

Es gab im vergangenen Jahr 5 Vorstandssitzungen, 54 Beschlüsse, 930 Mails auf der
Vorstands-Mailingliste, aber offenbar wenig Inhalt an sich. Die Vorstandssitzungen
sind weiterhin für alle Mitglieder offen (bis auf den nichtöffentlichen Teil).
Anträge bewegten sich eher in Richtung Anschaffungen als in Richtung
Mitgliedsangelegenheiten. 

\paragraph{Konflikt mit ehemaligem Mitglied}

Eine Angelegenheit mit einem ehemaligen Mitglied hat den Verein und den Vorstand dieses
Jahr sehr beschäftigt. Das Mitglied hatte seine Mitgliedschaft im Dezember (noch in
Kommunikation mit dem alten Vorstand) niedergelegt, hatte kurz darauf aber
einen Antrag auf Ausschluss eines anderen Mitglieds gestellt,
welcher vom (alten) Vorstand abgelehnt wurde.
Als Antwort verfasste das ehemalige Mitglied nun eine lange (vereinsöffentliche) Mail mit
Anschuldigungen und Beleidigungen gegenüber mehreren aktiven und ehemaligen
Vorstandsmitgliedern und Vereinsmitgliedern.

Der (nun neu gewählte) Vorstand erteilte dem ehemaligen Mitglied daraufhin Hausverbot und verfasste eine
Stellungnahme auf dem Normalverteiler. Vermutlich haben so alle Mitglieder diese
Angelegenheit direkt oder indirekt mitbekommen.

Im Laufe des Jahres wurde von dem ehemaligen Mitglied eine Strafanzeige gegen ein weiteres
Mitglied des Vereins gestellt. Diese wurde aber schlussendlich eingestellt.

Der Vorstand hat in dieser Sache viel Zeit investiert und mit Mitgliedern sowie dem
ehemaligen Mitglied Gespräche geführt, mit dem Ziel, die Anschuldigungen aufzuarbeiten.
Alle Kommunikation zu dem Thema füllen bisher mehrere Bücher, aber der Vorstand sieht nach
allen Gesprächen keine Grundlage für die vom ehemaligen Mitglied aufgestellten
Anschuldigungen.

Ebenso besteht aus Sicht des Vorstands im Moment auch keinen Grund, das ausgesprochene
Hausverbot gegenüber dem ehemaligen Mitglied aufzuheben. Das Hausverbot wird vom Vorstand
weiterhin als ein letztes Mittel der Konfliktlösung betrachtet, und wurde unter der
Kondition aufgestellt, dass das ehemalige Mitglied sein verletzendes und rufschädigendes
Verhalten gegenüber den Beschuldigten einsieht und sich entschuldigt. Da dies bisher
nicht eingetreten ist, und der Vorstand durch den festgefahrenen Konflikt die offene
Umgebung des Spaces in diesem Fall gefährdet sieht, bleibt das Hausverbot im Moment
weiterhin bestehen.

Ende des Jahres erreichte den Vorstand schließlich noch ein Brief eines Anwalts, der den
Austritt des ehemaligen Mitgliedes annullierte und hilfsweise eine Wiederaufnahme
beantragte. Der Vorstand lehnte – nach Rechtsberatung durch einen Anwalt seinerseits –
auch diese beiden Anträge mit Hinweis auf die o.g. Gründe ab. Für die Rechtsberatung wurde
ein Betrag von 500€ veranschlagt, über den die Mitglieder im Januar über den
Normalverteiler informiert wurden.

Da das Thema emotional diskutiert wurde, bietet der Vorstand sich weiterhin als
Gesprächspartner an, und verweist ebenso auf die Vertrauensperson.


Bericht der Vertrauensperson
----------------------------

dStulle berichtet als Vertrauensperson von dem selben Konflikt.
Die Problematik reicht bis 2017 und darüber hinaus in den Ursprüngen zurück, es gibt viel
Kontext zu verstehen und viel aufzuarbeiten. Die Vertrauensperson kann hier beide Seiten
verstehen, und hat auch das Gespräch mit dem Vorstand gesucht.

Ein zweiter, unabhängiger Fall hat sich dann mit der Zeit von selbst erledigt.

Als Konsequenz wird empfohlen, bei großen Problemen lieber frühzeitig das Gespräch suchen.
Nicht zu kommunizieren kann zu Verschlimmerung der Situation führen.

------

_Um 15:30 gibt es eine kurze Pause._

_Nach der Pause wird ein neu angekommenes Mitglied akkreditiert:_

* _33 = Anzahl anwesende ordentliche Mitglieder_
* _17 > 16.5 = 50% der anwesenden ordentlichen Mitglieder für Annahme eines Antrags (Satzung, §7 Abs. 7)_

_Wiederbeginn um 15:40._

------

Entlastung des Vorstandes
=========================

Als Wahlleitung wird ktrask vorgeschlagen. Es gibt keine Gegenstimmen oder Enthaltungen.
ktrask übernimmt die Versammlungsleitung.

Die Wahlleitung fragt, ob der Vorstand als Gesamtorgan entlastet werden soll, oder ob
jemand eine Einzelentlastung fordert. Letzteres ist nicht der Fall.

ktrask beantragt die Entlastung des Vorstand als Gesamtorgan. Es wird per Handzeichen
abgestimmt, und der Vorstand wird ohne Gegenstimmen und mit 7 Enthaltungen (Vorstand und
eine weitere Person) entlastet.

ktrask übergibt die Versammlungsleitung zurück an larsan.

Community/Vereinsleben
======================

Wie oben im Jahresbericht angesprochen, bewegt sich im Moment nicht viel.  Da der Vorstand
im Idealfall die Vereinsmitglieder vertreten soll, hätte er gerne Feedback zu einigen
Punkten. Es entsteht ein loses Brainstorming.

* Ein Mitglied äußert sich: Der Vorstand macht super Arbeit, um sich zu kümmern, aber ich
  würde gerne mehr aus der Basis heraus entscheiden. Viele andere Hackerspaces machen das
  auch so, ich würde mir wünschen, dass es auch bei uns nochmal versucht wird.
* Weiß jemand, wann das letzte Vorstands-Arbeitstreffen war? Wird das vielleicht zu
  schlecht kommuniziert? Muss der Vorstand mehr dafür werben?
* Jemand hatte mal Zettel ausgelegt, was im Space getan werden muss (nach einem der
  Space-Bau-Abend), das schafft Übersicht und Initiative
* Ticketsystem für Aufgaben einführen? 
  * gibt es schon: <https://gitli.stratum0.org/stratum0/TUWAT/issues>
* Worum geht es hier gerade, soll mehr gehackt werden? (Im Sinne von: mehr Workshops, mehr
  lose oder abgesprochene Treffen im Space, mehr Projekte, etc.)
  * Ja auch, aber z.B. auch mehr Initiative für Anschaffungen. 
  * Beispiel: jemand fährt in den Baumarkt und braucht ein paar Schrauben, dann kann man
    auch gleich einen großen Packen Schrauben kaufen, damit die nächsten nicht nochmal in
    den Baumarkt müssen. Aber Antragsbürokratie steht im Moment im Weg.
  * Freifunk verwaltet seine Spendengelder selbst (laut Vorstandsbeschluss)
  * in anderen Spaces gibt es z.B. Pauschalregelungen, dass Mitglieder Dinge bis z.B. 20€
    einfach so kaufen können und erstattet bekommen
    * so eine Regelung haben wir für Verbrauchsmaterial (Putzmittel, Toilettenpapier etc.)
      auch schon
    * aber es geht nicht nur um Verbrauchsmaterial, sondern auch z.B. um Werkzeug
  * es geht darum, besser und schneller zu entscheiden, als erst über den Vorstand gehen
    zu müssen
  * Vorschlag: wenn 7 Mitglieder zustimmen, dann ist es beschlossen
* Status Quo ist eingetreten. Ist das alles, was wir jemals erreichen werden, oder können
  wir noch aktiver wachsen?
* Kapazitätsgrenze Space 2.0, parallele Veranstaltungen machen den Space voll. Selbst
  die aktuelle Mitgliederversammlung sitzt sehr beengt.
* Was ist unser Ziel: So bleiben wie jetzt? Wachsen? Nach Space 3.0 Ausschau halten?
  Werkstatt ausbauen? Plattform für noch mehr Projekte wie Freifunk, Home Automation,
  Digitalcourage etc. sein?
  * Ausschau nach Space 3.0 war schon auf der letzten MV Thema, ist unkoordiniert
    verfolgt worden, es gab aber keine Arbeitsgruppe dafür
  * einigermaßen ordentlich erreichbar sollte es sein, Uninähe, mit Fahrrad und Öffis
    erreichbar, und leistbar für uns (keine 13€/m²)
* andere Frage: als was sieht sich der Space denn aktuell? Als Koch- und Social Space,
  oder als Elektronik-Labor, oder als…?
  * Social Space und Werkstatt und Elektroniklabor und „mal in die Runde fragen, wenn man
    was nicht weiß und es antwortet jemand“, alles zusammen ist der Sweet Spot.
  * es gibt viele Facetten: Holz, Plastik, Metall, aber räumliche Grenzen und die Treppe
    im Flur ist im Weg, wenn man Fahrräder in der Werkstatt reparieren will
    * drei Leute gleichzeitig in der Werkstatt ist schon sehr eng
  * mit Leuten labern und (nicht so) dumme Ideen austauschen und auf neue Projektideen
    kommen und auch am selben Abend noch umsetzen
  * Ich kann alles zu Hause machen, aber es macht alleine nur halb so viel Spaß
    * „Hacker-WG“
  * mal schnell was aus dem Boden stampfen (HOA?) geht auf das Vertrauensverhältnis im
    Space zurück
  * wir sind eine Plattform für Projekte: CoderDojo, Freifunk, Digitalcourage, aber da ist
    eindeutig Platzbedarf vorhanden
  * es gibt keinen Raum, um konzentriert zu arbeiten, parallele Veranstaltungen im Space
    stören sich gegenseitig bzw. stören die Leute, die dort „unangemeldet“ arbeiten
    wollen
* Kritik im Moment sind die begrenzten Räumlichkeiten für größere Projekte 
* heißt das im Umkehrschluss: die großen Projekte kommen nicht, weil der Platz nicht da
  ist?
* sind wir aktuell zu viele Projekte für den Platz und die menschlichen Ressourcen?
* Platzproblem ist teilweise auch ein Verteilungsproblem (eine Entität / eine Party pro Raum)
* die einzelnen Räume sind spezialisiert (Beamer/Sofas, Werkstatt, Küche); wenn ein Raum
  besetzt ist, schließt das eine andere Nutzung aus
  * Beispiel: Projekt Parker (Freifunk) ist auf eine private WG ausgelagert worden, aber
    dann fehlen Netzwerkeffekte
* ein Meeting-Raum wäre gut
* die Projekte, die sich hier treffen, respektieren und sie nicht bzw. weniger stören
  * ein paar Projekte auch viel mit externen Leuten und Nicht-Mitgliedern besetzt
    * führt das zu einem finanziellen Problem? Anspruch an Räumlichkeiten besteht, aber
      muss sich (finanziell für den Verein) rechnen (was es im Moment noch tut)
* laut der Hackerspace API ist Stratum 0 Spitzenreiter in Bezug auf Veranstaltungen, aber
  wir haben nur mittelgroßen Platz
* externe Projekte in der Satzung o.ä. regeln und so Verbindlichkeit herstellen?
* an regelmäßige Projekte kommunizieren, dass auch ein finanzieller Beitrag erwartet wird?
* wenn Mitglieder Raum benötigen, ihnen den Vorrang vor Externen einräumen?
* es geht nicht darum, (externen) Projekten den Platz wegzunehmen

Der Vorstand nimmt die Punkte erst einmal so mit.

Wahlen
======

Die Versammlungsleitung geht wieder an die Wahlleitung, die den Wahlmodus erläutert.
Gewählt wird nach der Geschäftsordnung der Mitgliederversammlung.[^gomv]

[^gomv]: <https://stratum0.org/wiki/Mitgliederversammlung/Gesch%C3%A4ftsordnung>

Es werden Kandidaten für die Vorstandsposten und die Rechnungsprüfer gesucht. Nach kurzem
Meinungsbild wird die Wahl für die Vertrauensperson später separat vorgenommen.
Es melden sich mehrere Mitglieder für die Posten und stellen sich kurz vor.

Danach werden die Wahlzettel gedruckt und 33 Wahlzettel werden ausgegeben.
Die Wahlzettel haben leider das Datum vom 4. Dezember 2016, aber das ist erst zu spät
aufgefallen.

Die Stimmabgabe wird um 16:39 geschlossen. 
Als Wahlhelfer für die Auszählung melden sich shim, tilman und chaosgrille.

------

_Zwei Mitglieder verlassen die Versammlung während der Auszählung._

* _30 = Anzahl anwesende ordentliche Mitglieder_
* _16 > 15 = 50% der anwesenden ordentlichen Mitglieder für Annahme eines Antrags (Satzung, §7 Abs. 7)_

------

ktrask verkündet das Ergebnis nach der Auszählung. Es sind 33 Wahlzettel eingegangen.

\paragraph{Vorstandsvorsitzender}

+----------------------------+---------------+
| Kandidat                   | Stimmenanzahl |
+============================+===============+
| larsan                     | 33            |
+----------------------------+---------------+

larsan (Lars Andresen) ist mit 33 von 33 Stimmen (100%) als Vorstandsvorsitzender gewählt,
und nimmt die Wahl an.

\paragraph{Stellvertretender Vorsitzender}
+----------------------------+---------------+
| Kandidat                   | Stimmenanzahl |
+============================+===============+
| reneger                    | 27            |
+----------------------------+---------------+
| Coldney                    | 12            |
+----------------------------+---------------+

reneger (René Stegmaier) ist mit 27 von 33 Stimmen (81%) als stellvertretender
Vorsitzender gewählt, und nimmt die Wahl an.

\paragraph{Schatzmeister}
+----------------------------+---------------+
| Kandidat                   | Stimmenanzahl |
+============================+===============+
| Emantor                    | 33            |
+----------------------------+---------------+

Emantor (Rouven Czerwinski) ist mit 33 von 33 Stimmen (100%) als Schatzmeister gewählt,
und nimmt die Wahl an.

\paragraph{Beisitzer}
+----------------------------+---------------+
| Kandidat                   | Stimmenanzahl |
+============================+===============+
| Linda / chamaeleon         | 27            |
+----------------------------+---------------+
| ChaosRambo                 | 23            |
+----------------------------+---------------+
| rohieb                     | 21            |
+----------------------------+---------------+
| chrissi^                   | 21            |
+----------------------------+---------------+
| reneger                    | 18            |
+----------------------------+---------------+
| lichtfeind                 | 17            |
+----------------------------+---------------+
| Angela                     | 17            |
+----------------------------+---------------+
| Coldney                    | 14            |
+----------------------------+---------------+
| dadada                     | 9             |
+----------------------------+---------------+
 
chamaeleon (Linda Fliß) ist mit 27 von 33 Stimmen (81%) und ChaosRambo (Daniel Thümen) mit
23 von 33 Stimmen (69%) als Beisitzer gewählt. Beide nehmen die Wahl an.

rohieb zieht seine Kandidatur zurück, damit bleibt chrissi^ (Chris Fiege) als gewählter
dritter Beisitzer mit 21 von 33 Stimmen (63%). chrissi^ nimmt die Wahl zum Beisitzer an.

\paragraph{Rechnungsprüfer}
+----------------------------+---------------+
| Kandidat                   | Stimmenanzahl |
+============================+===============+
| shoragan                   | 31            |
+----------------------------+---------------+
| Angela                     | 31            |
+----------------------------+---------------+
| lichtfeind                 | 18            |
+----------------------------+---------------+

Angela und shoragan sind mit jeweils 31 von 33 Stimmen (93%) als Rechnungsprüfer gewählt.
shoragan nimmt die Wahl an.
Angela hat die Versammlung inzwischen verlassen, wird von der Wahlleitung angerufen, und
nimmt die Wahl telefonisch an.

\paragraph{Vertrauensperson}

dStulle stellt sich weiterhin als Kandidat für die Vertrauensperson zur Verfügung.
Es soll per Handzeichen abgestimmt werden, niemand möchte geheim wählen, der Rest ist
mehrheitlich für Wahl durch Handzeichen.

dStulle wird mit 3 Enthaltungen und ohne Gegenstimmen zur Vertrauensperson gewählt, und
nimmt die Wahl an.

Nachträglich wird die Frage gestellt, wer eigentlich diese Vertrauensperson ist, und warum
das Amt nicht in der Satzung steht?
Die Antwort lautet (wie letztes Mal): Das Amt wurde spontan auf einer der letzten
Mitgliederversammlung geschaffen und wird formlos besetzt.

Es wird der Antrag gestellt, die Mitgliederversammlungen möge den Vorstand beauftragen,
nachzuvollziehen, ob das Amt der Vertrauensperson in der Satzung stehen muss.
Über den Antrag wird per Handzeichen abgestimmt. Der Antrag wird mit 9 Pro-Stimmen, 14
Enthaltungen und 3 Contra-Stimmen abgelehnt.
Die Versammlungsleitung merkt an, dass jedem Mitglied ein Satzungsänderungsantrag für
folgende Mitgliederversammlungen frei steht.

Sonstiges
=========

Folgende Punkte werden aus Zeitmangel vertagt:

* Weltherrschaft
* Anerkennung des RZL als Außenstelle des Stratum 0
* Key-Signing-Party
