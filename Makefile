#!/usr/bin/make -f

# Hier die Zieldateien eintragen, die gebaut werden sollen:
OBJ = \
      protokoll-mv-2019-01-20.pdf \

# Ab hier muss eigentlich nichts mehr geändert werden.

# Macro für Pandoc:
pandoc = pandoc -f markdown+grid_tables -i $< -s -t $(1) -o $@ $(2)

all: $(OBJ)

.git/modules/latex/: .gitmodules
	git submodule update --init

s0artcl.cls s0minutes.cls: .git/modules/latex/
	cd latex && make
	rm -f $@ && ln -s $(patsubst %,latex/%,$@) .
	touch $@

%.pdf: %.latex s0minutes.cls s0artcl.cls
	pdflatex $< \
	&& pdflatex $< \
	&& pdflatex $<

%.latex: %.md pandoc-template.latex
	$(call pandoc, latex, --template=pandoc-template)

clean:
	rm -f $(OBJ) $(OBJ:pdf=latex) $(OBJ:pdf=aux) $(OBJ:pdf=log) $(OBJ:pdf=out) $(OBJ:pdf=toc)
	rm -f s0artcl.cls s0minutes.cls
